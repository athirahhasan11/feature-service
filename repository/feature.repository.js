const { sequelize } = require("../utils/database");
const FeatureModel = require("../models/feature")(sequelize);

module.exports.listFeatureByEmailAndFeature = async (email, featureName) => {
  return await FeatureModel.findOne({
    where: {
      email: email,
      featureName: featureName,
    },
  });
};

module.exports.createFeature = async (feature, transaction) => {
  return await FeatureModel.create(feature, { transaction });
};
