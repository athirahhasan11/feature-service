const Sequelize = require("sequelize");

module.exports = (sequelize) => {
  class Feature extends Sequelize.Model {}
  Feature.init(
    {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
      },
      email: {
        type: Sequelize.STRING,
      },
      featureName: {
        type: Sequelize.STRING,
      },
      enable: {
        type: Sequelize.BOOLEAN,
      },
    },
    {
      sequelize,
      underscored: true,
      timestamps: false,
      modelName: "features",
    }
  );
  return Feature;
};
