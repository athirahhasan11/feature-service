const { DatabaseError, createError } = require("../utils/error");
const featureRepo = require("../repository/feature.repository");
const logger = require("../utils/logger");

module.exports.addFeatures = async (featureReq, transaction) => {
  try {
    const feature = await featureRepo.listFeatureByEmailAndFeature(
      featureReq.email,
      featureReq.featureName
    );

    if (!feature) {
      logger.info(`Record will insert in database`);
      return await featureRepo.createFeature(featureReq, transaction);
    } else {
      throw new Error(`Record ${feature.id} already exist`);
    }
  } catch (error) {
    throw createError("Unable to add features.", error);
  }
};

module.exports.listFeature = async (reqFeature) => {
  try {
    const featureAccess = await featureRepo.listFeatureByEmailAndFeature(
      reqFeature.email,
      reqFeature.featureName
    );

    if (featureAccess) {
      return { canAccess: featureAccess.enable };
    }
  } catch (error) {
    throw createError(`Unable to retrieve the feature access.`, error);
  }
};
