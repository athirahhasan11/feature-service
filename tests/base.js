require('should');
const supertest = require('supertest');
const { sequelize, event } = require('../utils/database');
const app = require('../index');
const agent = supertest.agent(app);

// Ensure Express App and init access token
before(async () => {
 
  await new Promise(resolve => {
    app.on('appStarted', () => {
      resolve();
    });
  });
});

module.exports.agent = agent;
module.exports.app = app;
module.exports.sequelize = sequelize;
