const base = require("./base");
const Feature = require("../models/feature")(base.sequelize);

describe("Feature API", () => {
  const feature = {
    email: "test@gmail.com",
    featureName: "support",
    enable: true,
  };

  it("should not return or direct to path when no parameters", async () => {
    await base.agent.get("/feature").expect(404);
  });

  it("should return empty when no valid user", async () => {
    const resultGet = await base.agent
      .get(
        `/api/feature?email=${feature.email}&featureName=${feature.featureName}`
      )
      .expect(200);
    resultGet.body.length.should.equal(0);
  });

  it("should Post data to feature", async () => {
    await base.agent
      .post(`/api/feature`)
      .send(feature)
      .expect(200);
  });

  it("should not Post data to feature", async () => {
    await base.agent
      .post(`/api/feature`)
      .send(feature)
      .expect(304);
  });

  it("should return access body of the user ", async () => {
    const resultGet = await base.agent
      .get(
        `/api/feature?email=${feature.email}&featureName=${feature.featureName}`
      )
      .expect(200);
    resultGet.body.should.have
      .property("canAccess")
      .and.be.equal(feature.enable);
  });

  after(async () => {
    await Feature.destroy({
      where: { email: feature.email },
    });
  });
});
