/**
 * Purpose of a config file:
 * - clarity on how all environment variables are being mapped
 * - you can rename variables to more readable properties
 * - you can add other configuration properties from non-environment variables
 *
 * The recommended way to change any environment variable on your dev machine is:
 * - add a '.env' on the project root folder /file-management-service
 * - set all environment variables as can be seen on the '.env.example' file
 */
const config = {
 nodeEnv: process.env.NODE_ENV,
  serverPort: process.env.SERVER_PORT,
  logLevel: process.env.LOG_LEVEL,
  mysqlHostname: process.env.MYSQL_HOST_NAME,
  mysqlPort: process.env.MYSQL_PORT,
  mysqlDatabase: process.env.MYSQL_DATABASE,
  mysqlUsername: process.env.MYSQL_USER_NAME,
  mysqlPassword: process.env.MYSQL_PASSWORD,

};

/**
 * Using console.log to output the env variables because it is a synchronous call
 * and at this poin the logger is not ready (because logger depends on config)
 */
// eslint-disable-next-line no-console
console.log(
  `Environment variables provided:
  - process.env.NODE_ENV: ${config.nodeEnv}
  - process.env.SERVER_PORT: ${config.serverPort}
  - process.env.LOG_LEVEL: ${config.logLevel}
  - process.env.MYSQL_HOST_NAME: ${config.mysqlHostname}
  - process.env.MYSQL_PORT: ${config.mysqlPort}
  - process.env.MYSQL_DATABASE: ${config.mysqlDatabase}
  - process.env.MYSQL_USER_NAME: ${config.mysqlUsername}
  - process.env.MYSQL_PASSWORD: ${config.mysqlPassword}
 `
);

module.exports = config;