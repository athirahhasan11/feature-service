# Feature Management Service
The Service that allow for product manager to manage user 
### Description
This is backend service that manage user and features. 

### Installation
 Here are the steps necessary to setup your dev environment to run the entire application (Node/Express + MySQL) on the local.

1. Checkout code
```
git clone https://gitlab.com/athirahhasan11/feature-service.git
```
3. Copy default env config
```
cp .env.example .env
```
4. Access the Swagger Doc UI interface
````
http://127.0.0.1:5000/docs
````

5. This is it!!! You are ready to start your implementation. Any source code change will automatically trigger a restart in the serve application

## FAQ

*  [Why should I update the .env.example file?](https://medium.com/the-node-js-collection/making-your-node-js-work-everywhere-with-environment-variables-2da8cdf6e786)
