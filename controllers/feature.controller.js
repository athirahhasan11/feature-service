const featureService = require("../services/feature.service");
const { pipeMiddlewares } = require("../utils/pipeMiddlewares");
const { sequelize } = require("../utils/database");
const {
  validateGetFeatureParam,
  validatePostFeatureParam,
} = require("../validator/feature.validator");

module.exports.getFeaturesByEmailAndFeature = pipeMiddlewares([
  validateGetFeatureParam,
  async (req, res) => {
    try {
      const featureAccess = await featureService.listFeature(
        req.swagger.params.validatedParam
      );
      res.status(200).json(featureAccess);
    } catch (error) {
      res.status(400).json(error);
    }
  },
]);

module.exports.insertFeature = pipeMiddlewares([
  validatePostFeatureParam,
  async (req, res) => {
    const { value: feature } = req.swagger.params.body;
    const transaction = await sequelize.transaction();
    try {
      await featureService.addFeatures(feature, transaction);
      await transaction.commit();
      res.status(200).end();
    } catch (error) {
      await transaction.rollback();
      res.status(304).json(error);
    }
  },
]);
