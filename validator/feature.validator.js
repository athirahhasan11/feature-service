const Joi = require("@hapi/joi");
const _ = require("lodash");
const logger = require("../utils/logger");

const getFeatureSchema = Joi.object().keys({
  email: Joi.string()
    .email()
    .required(),
  featureName: Joi.string().required(),
});

const postFeatureSchema = Joi.object().keys({
  email: Joi.string()
    .email()
    .required(),
  featureName: Joi.string().required(),
  enable: Joi.boolean().required(),
});

module.exports.validateGetFeatureParam = async (req, res, next) => {
  const { params } = req.swagger;
  const getFeatureParam = {
    email: params.email && params.email.value ? params.email.value : "",
    featureName:
      params.featureName && params.featureName.value
        ? params.featureName.value
        : "",
  };
  try {
    const validatedParam = await getFeatureSchema.validateAsync(
      getFeatureParam
    );
    req.swagger.params.validatedParam = _.cloneDeep(validatedParam);
    next();
  } catch (error) {
    logger.error("Field validation error when get feature parameter,", error);
    res.status(400).send(error);
  }
};

module.exports.validatePostFeatureParam = async (req, res, next) => {
  const { value: Feature } = req.swagger.params.body;
  try {
    await postFeatureSchema.validateAsync(Feature);
    next();
  } catch (error) {
    logger.error("Field validation error when post distribution list,", error);
    res.status(400).send(error);
  }
};
