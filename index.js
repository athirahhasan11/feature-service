const fs = require("fs");
const path = require("path");
const http = require("http");
const express = require("express");
const bodyParser = require("body-parser");
const swaggerTools = require("swagger-tools");
const swaggerUiDist = require("swagger-ui-dist");
const replace = require("replace-in-file");
const jsyaml = require("js-yaml");
const cors = require("cors");
const config = require("./config");
const logger = require("./utils/logger");
const delay = (time) => new Promise((res) => setTimeout(res, time));

// swaggerRouter configuration
const options = {
  swaggerUi: path.join(__dirname, "/swagger.json"),
  controllers: path.join(__dirname, "./controllers"),
  useStubs: config.nodeEnv === "development", // Conditionally turn on stubs (mock mode)
};

// The Swagger document
const spec = fs.readFileSync(path.join(__dirname, "api/swagger.yaml"), "utf8");
const swaggerDoc = jsyaml.safeLoad(spec);

// Creates an Express application
const app = express();

// Initialize the Swagger middleware
swaggerTools.initializeMiddleware(swaggerDoc, async (middleware) => {
  //give time to load the other configuration due to no authentication to implement.
  await delay(1000);

  // Accept all cors
  app.use(cors());

  app.use(bodyParser.json({}));

  // Interpret Swagger resources and attach metadata to request
  // must be first in swagger-tools middleware chain
  app.use(middleware.swaggerMetadata());

  // Validate Swagger requests
  app.use(middleware.swaggerValidator());

  // Route validated requests to appropriate controller
  app.use(middleware.swaggerRouter(options));

  // Serve the Swagger documents and Swagger UI
  const swaggerUiDir = swaggerUiDist.getAbsoluteFSPath();
  replace.sync({
    files: path.join(swaggerUiDir, "index.html"),

    from: "https://petstore.swagger.io/v2/swagger.json",
    to: "/api-docs",
  });
  app.use(middleware.swaggerUi({ swaggerUiDir }));

  // Server health check
  app.get("/health", (req, res) => {
    res.status(200).json({});
  });

  http.createServer(app).listen(config.serverPort, () => {
    logger.info(
      `Your server is listening on port(http://localhost:${config.serverPort})`
    );
    logger.info(
      `Swagger-ui is available on http://localhost:${config.serverPort}/docs`
    );
    app.emit("appStarted");
  });
});

module.exports = app;
