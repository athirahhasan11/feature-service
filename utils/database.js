const Sequelize = require("sequelize");
const Umzug = require("umzug");
const path = require("path");
const events = require("events");
const config = require("../config");
const logger = require("./logger");

const Db = {};
Db.event = new events.EventEmitter();

const connection = new Sequelize(
  config.mysqlDatabase,
  config.mysqlUsername,
  config.mysqlPassword,
  {
    host: config.mysqlHostname,
    port: config.mysqlPort,
    dialect: "mysql",
    define: {
      charset: "utf8",
      collate: "utf8_general_ci",
      freezeTableName: true,
    },
    logging: (msg) => logger.info(msg),
  }
);

const umzug = new Umzug({
  storage: "sequelize",

  storageOptions: {
    sequelize: connection,
  },

  migrations: {
    params: [connection.getQueryInterface(), Sequelize],
    path: path.join(__dirname, "./../migrations"),
  },
});

umzug
  .up()

  .then(() => {
    Db.event.emit("dbStarted");
    logger.info(
      `MySQL database have been successfully synced with the following parameters:
      - Hostname: ${config.mysqlHostname}
      - Port: ${config.mysqlPort}
      - Database name: ${config.mysqlDatabase}`
    );
  })
  .catch((error) => {
    logger.error("Unable to sync MySQL database", error);
  });

Db.sequelize = connection;

module.exports = Db;
