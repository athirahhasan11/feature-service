const winston = require("winston");
const config = require("../config");

winston.emitErrs = true;

const LOG_MAX_LENGTH = 8000;

/* The logger has the ability to output entries on the console and in a log file rotation.
 * But, the DECC implementation recomends outputting all entries in the console only.
 */
const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      level: config.logLevel,
      handleExceptions: true,
      json: false,
      colorize: true,
      timestamp: true,
    }),
    // new winston.transports.File({
    //   level: 'info',
    //   filename: './logs/all-logs.log',
    //   handleExceptions: true,
    //   json: true,
    //   maxsize: 5242880, // 5MB
    //   maxFiles: 5,
    //   colorize: false,
    // }),
  ],
  exitOnError: false,
});

module.exports.stream = {
  // eslint-disable-next-line no-unused-vars
  write: (message, encoding) => {
    logger.info(message);
  },
};
module.exports.info = (msg, arg) => {
  const fullMsg = msg + (arg ? JSON.stringify(arg) : "");
  if (fullMsg.length > LOG_MAX_LENGTH) {
    const re = new RegExp(`.{1,${LOG_MAX_LENGTH}}`, "g");
    fullMsg.match(re).forEach((subStr) => logger.info(subStr));
  } else {
    logger.info(msg, arg);
  }
};

module.exports.error = (msg, arg) => {
  const fullMsg = msg + (arg ? JSON.stringify(arg) : "");
  if (fullMsg.length > LOG_MAX_LENGTH) {
    const re = new RegExp(`.{1,${LOG_MAX_LENGTH}}`, "g");
    fullMsg.match(re).forEach((subStr) => logger.error(subStr));
  } else {
    logger.error(msg, arg);
  }
};
