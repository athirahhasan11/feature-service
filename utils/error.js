const Sequelize = require("sequelize");

class ApplicationError extends Error {
  constructor(message, status, originError) {
    super();
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message || "Something went wrong. Please try again.";
    this.status = status || 500;
    this.originError = originError;
  }
}

class NotFoundError extends ApplicationError {
  constructor(message, originError) {
    super(message || "Not found.", 404, originError);
  }
}

class FieldValidationError extends ApplicationError {
  constructor(message, originError) {
    super(
      message || "Parameter validation failed, please check.",
      400,
      originError
    );
  }
}

class UnauthorizedError extends ApplicationError {
  constructor(message, originError) {
    super(
      message || "User does not have permission to perform the operation.",
      403,
      originError
    );
  }
}

class DatabaseError extends ApplicationError {
  constructor(message, originError) {
    super(
      message || "An error occurred while performing a database operation.",
      304,
      originError
    );
  }
}

class ObtainTokenError extends ApplicationError {
  constructor(message, originError) {
    super(message || "Failed to get token.", 503, originError);
  }
}

class NoeServiceError extends ApplicationError {
  constructor(message, originError) {
    super(
      message || "Unable to send email notification using NOE service",
      503,
      originError
    );
  }
}

class AuthorizationServiceError extends ApplicationError {
  constructor(message, originError) {
    super(
      message || "An error occurred while accessing the Authorization service",
      503,
      originError
    );
  }
}

const createError = (message, originError) => {
  if (originError instanceof Sequelize.UniqueConstraintError) {
    return new DatabaseError(
      "Already have the same element, please use another name.",
      originError
    );
  }
  if (originError instanceof Sequelize.ConnectionError) {
    return new DatabaseError(
      "Unable to connect to the database, please try again later.",
      originError
    );
  }
  if (originError instanceof Sequelize.Error) {
    return new DatabaseError(null, originError);
  }
  if (originError instanceof ApplicationError) {
    return originError;
  }
  return new ApplicationError(message, 500, originError);
};

module.exports = {
  createError,
  ApplicationError,
  FieldValidationError,
  NotFoundError,
  UnauthorizedError,
  DatabaseError,
  ObtainTokenError,
  NoeServiceError,
  AuthorizationServiceError,
};
