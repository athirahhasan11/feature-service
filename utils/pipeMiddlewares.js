/**
 * This class exports a function that is capable to pipe middlewares (using stacks).
 * This is quite usefull when you are working with swagger tools and controllers.
 * More info see: https://github.com/swagger-api/swagger-node/issues/278
 * */

module.exports.pipeMiddlewares = (middlewares, resource, action) => {
  return (req, res, next) => {
    const stack = [];
    for (let i = middlewares.length - 1; i >= 0; i -= 1) {
      const middleware = middlewares[i];
      if (i === 0) {
        try {
          middleware(
            req,
            res,
            stack[stack.length - i - 1],
            resource,
            action
          ).then(
            (_) => {},
            (err) => {
              next(err);
            }
          );
        } catch (error) {
          next(error);
        }
      } else if (i === middlewares.length - 1) {
        stack.push(() => {
          middleware(req, res, next).then(
            (_) => {},
            (err) => {
              next(err);
            }
          );
        });
      } else {
        stack.push(() => {
          middleware(
            req,
            res,
            stack[stack.length - i - 1],
            resource,
            action
          ).then(
            (_) => {},
            (err) => {
              next(err);
            }
          );
        });
      }
    }
  };
};
